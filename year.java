package Year;

import java.util.Scanner;

import static java.time.Year.isLeap;

public class year {

    public static void main(String[] args) {
        Scanner data = new Scanner(System.in);
        String dataMessage = data.next();
        String[] arr = dataMessage.split("\\.");
        boolean isLeap = isLeap(Integer.parseInt(arr[2]));
        if (isLeap) {
            System.out.println("Год високосный!");
        } else {
            System.out.println("Год не високосный!");
        }

    }
}
